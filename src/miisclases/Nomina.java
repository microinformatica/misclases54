/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miisclases;

/**
 *
 * @author patri
 */
public class Nomina {
    private int numNomina;
    private String nombre;
    private int nivel;
    private int pagoHora;
    private int horasTrabajadas;

    public Nomina() {
    }

    public Nomina(int numNomina, String nombre, int nivel, int pagoHora, int horasTrabajadas) {
        this.numNomina = numNomina;
        this.nombre = nombre;
        this.nivel = nivel;
        this.pagoHora = pagoHora;
        this.horasTrabajadas = horasTrabajadas;
    }

    public int getNumNomina() {
        return numNomina;
    }

    public void setNumNomina(int numNomina) {
        this.numNomina = numNomina;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getPagoHora() {
        return pagoHora;
    }

    public void setPagoHora(int pagoHora) {
        this.pagoHora = pagoHora;
    }

    public int getHorasTrabajadas() {
        return horasTrabajadas;
    }

    public void setHorasTrabajadas(int horasTrabajadas) {
        this.horasTrabajadas = horasTrabajadas;
    }
    
    
    
}
